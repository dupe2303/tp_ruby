class CreateCustomers < ActiveRecord::Migration[6.0]
  def change
    create_table :customers do |t|
      t.string :name
      t.date :relationshipStart
      t.string :adressCity
      t.string :adressPostalCode
      t.string :adressStreet
      t.string :adressApt
      t.integer :activityType
      t.string :infoEmail

      t.timestamps
    end
  end
end
