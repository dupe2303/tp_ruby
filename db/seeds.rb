# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
User.create!(email: "adupont@mail.com",
             password: "foobar",
             password_confirmation: "foobar")

User.create!(email: "adupont@example.com",
             password: "example",
             password_confirmation: "example")   
             
User.create!(email: "example@mail.com",
             password: "soleil",
             password_confirmation: "soleil")

Customer.create!(name: "precicom",
                  relationshipStart: DateTime.current.to_date,
                  adressCity: "Thetford Mines",
                  adressPostalCode: "G6G 6K2",
                  adressStreet: "233 Boulevard Frontenac O",
                  adressApt: "400",
                  activityType: 2,
                  infoEmail: "example@mail.com")