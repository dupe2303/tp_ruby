class Contact < ApplicationRecord
    belongs_to :customer
    VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
    validates :name, presence: true
    validates :firstname, presence: true
    validates :email, presence: true, format: { with: VALID_EMAIL_REGEX }
    validates :ext, numericality: true, presence: false
end
