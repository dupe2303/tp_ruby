class Customer < ApplicationRecord
    has_many :contact, :dependent => :restrict_with_error
    VALIDATE_POSTAL_REGEX = /\A[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1}[ -]?\d{1}[A-Z]{1}\d{1}\z/
    VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
    enum activityType: [:technologie, :alimentaire, :industriel, :divertissement, :autre]
    validates :adressPostalCode, format: {with: VALIDATE_POSTAL_REGEX}
    validates :name, presence: true
    validates :relationshipStart, presence: true
    validates :activityType, presence: true
    validates :infoEmail, presence: true, format: { with: VALID_EMAIL_REGEX }
    validates :adressApt, length: { maximum: 12 }
end
