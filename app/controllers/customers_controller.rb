class CustomersController < ApplicationController
  before_action :logged_in_user, only: [:index, :show, :edit, :update, :destroy]

  def new
    @customer = Customer.new
  end

  def index
    @customers = Customer.all
  end


  def show
    @customers = Customer.find(params[:id])
    @contacts = Contact.where("customer_id = ?", params[:id])
  end

  def create
    @customer = Customer.new(customer_params)
    if @customer.save
      redirect_to customers_path
    else
      render 'new'
    end
  end

  def edit
    @customer = Customer.find(params[:id])
  end
  
  def update
    @customer = Customer.find(params[:id])
    if @customer.update_attributes(customer_params)
      flash[:success] = "Customer updated"
      redirect_to customers_path
    else
      render 'edit'
    end
  end

  def destroy
    @customer = Customer.find(params[:id])

    if @customer.can_destroy?
      @customer.destroy
      flash[:success] = "Customer deleted"
    else
      flash[:danger] = "Customer can't be deleted because it has 1 or more contact"
    end
    redirect_to customers_path
  end

  private

  def customer_params
    params.require(:customer).permit(:name, :relationshipStart, :adressCity,
                                 :adressPostalCode, :adressStreet, :adressApt, :activityType, :infoEmail)
  end

  def logged_in_user
    unless logged_in?
      flash[:danger] = "Please log in."
      redirect_to root_url
    end
  end



end
