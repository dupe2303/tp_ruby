class ContactsController < ApplicationController
  before_action :logged_in_user, only: [ :edit, :update, :destroy]

  def new
    @contact = Contact.new
    @contact.customer_id = params[:customer_id]
    render 'new'
  end
  
  def edit
    @contact = Contact.find(params[:id])
  end

  def create
    @contact = Contact.new(contact_params)
    @customer = Customer.find_by(params[:id])
      if @contact.save
        flash[:success] = "Contact was successfully created."
        redirect_to customer_path(@customer.id)
      else
        render 'new'
      end
  end

  def update
      @customer = Customer.find_by(params[:id])
      if @contact.update_attributes(contact_params)
        flash[:success] = "Contact was successfully updated."
        redirect_to customer_path(@customer.id)
      else
        render 'edit'
      end
  end

  def destroy
    @contact = Contact.find(params[:id])
    @customer = Customer.find_by(params[:id])
    @contact.destroy
    flash[:success] = "Contact was successfully deleted."
    redirect_to customer_path(@customer.id)
  end

  private

  def logged_in_user
    unless logged_in?
      flash[:danger] = "Please log in."
      redirect_to root_url
    end
  end

    # Only allow a list of trusted parameters through.
    def contact_params
      params.require(:contact).permit(:name, :firstname, :email, :tel, :ext, :customer_id)
    end
end
